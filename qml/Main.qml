/*
 * Copyright (C) 2020-2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * einkaufszettel is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Lomiri.Components 1.3
import Qt.labs.settings 1.1
import MDTK.Manual 1.0 as MDTKManual
import MDTK 1.0 as MDTK

import "components"
import "db"
import "backend"

MainView {
    id: root
    objectName: 'mainView'
    applicationName: 'einkaufszettel.matdahl'
    automaticOrientation: true

    width: units.gu(45)
    height: units.gu(75)

    Component.onCompleted: pages.push(Qt.resolvedUrl("pages/MainPage.qml"))

    // the database connector which manage all interactions for entries
    EntriesManager{
        id: entries
    }

    // the database connector which manage all interactions for categories
    DBCategories{
        id: db_categories
    }

    // the database connector to store the history of entries if wanted
    DBHistory{
        id: db_history
    }

    // the colors object which stores all informations about current color theme settings
    MDTK.Colors{
        id: colors
        defaultIndex: 1
    }
    theme.name: colors.currentThemeName
    backgroundColor: colors.currentBackground

    // Settings
    Settings{
        id: settings
        property bool swapCheckBoxPosition: false
        property int  selectedList: 0
    }


    // the units to measure quantities of entries
    Dimensions{
        id: dimensions
    }

    // user interface
    PageStack{
        id: pages
    }

    /* ------ manual ------ */
    MDTKManual.Manual{
        id: manual
        dialogComponent: "file:qml/components/ManualDialog.qml"
    }
}
