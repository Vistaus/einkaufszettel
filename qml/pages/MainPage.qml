/*
 * Copyright (C) 2020-2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * einkaufszettel is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Lomiri.Components 1.3
import MDTK 1.0 as MDTK

import "../components"

MDTK.ThemedPage {
    id: root
    title: i18n.tr("Shopping Lists")
    headerTrailingActionBar.actions: [
        Action{
            iconName: "settings"
            onTriggered: {
                pages.push(Qt.resolvedUrl("SettingsPage.qml"))
            }
        },
        Action{
            iconName: "select-none"
            visible: entries.hasChecked
            onTriggered: entries.deselectAll()
        }
    ]

    QtObject{
        id: layoutProperties

        property int quantityWidth: units.gu(2)

        readonly property bool viewMixedCategories: sections.selectedIndex === 0 // list "all"
                                                    || sections.selectedIndex === db_categories.model.count+1 // list "other"
    }

    Sections{
        id: sections
        anchors{
            top:   header.bottom
            left:  parent.left
            right: parent.right
        }
        height: units.gu(6)
        Component.onCompleted: {
            db_categories.categoriesChanged.connect(update)
            update()
        }
        selectedIndex: settings.selectedList
        onSelectedIndexChanged: {
            if (selectedIndex <= 0)
                entries.updateSelectedCategory("",false)
            else if (selectedIndex > db_categories.model.count)
                entries.updateSelectedCategory("",true)
            else
                entries.updateSelectedCategory(db_categories.model.get(selectedIndex-1).name,false)
            settings.selectedList = selectedIndex
        }
        function update(){
            // check if sections need to be reduced
            var index = -2
            if (db_categories.model.count+2 < actions.length){
                index = selectedIndex
                for (var j=actions.length-1; j>0; j--)
                    actions[j].destroy()
                actions = []
            }

            // check if sections need to be added
            if (db_categories.model.count+2 > actions.length){
                index = selectedIndex
                for (var i=actions.length; i<db_categories.model.count+2; i++){
                    actions.push(Qt.createQmlObject("import Lomiri.Components 1.3; Action{text:'test "+i+"'}",sections))
                }
            }
            if (index !== -2 && index < actions.length)
                selectedIndex = index

            // update texts
            actions[0].text = db_categories.countAll>0 ? "<b>" + i18n.tr("all") + " ("+db_categories.countAll+")</b>"
                                                       : i18n.tr("all") + " (0)"
            actions[db_categories.model.count+1].text = db_categories.countOther>0 ? "<b>" + i18n.tr("other") + " ("+db_categories.countOther+")</b>"
                                                                                   : i18n.tr("other") + " (0)"
            for (var k=0; k<db_categories.model.count; k++){
                var count = db_categories.model.get(k).count
                var txt = count>0 ? "<b>" : ""
                txt += db_categories.model.get(k).name + " ("+count+")"
                txt += count>0? "</b>" : ""
                actions[k+1].text = txt
            }
        }
    }

    Label{
        anchors.centerIn: parent
        text: "("+i18n.tr("No entries") +")"
        visible: listView.model.count===0
    }

    LomiriListView{
        id: listView
        anchors{
            top:    inputRow.bottom
            bottom: root.bottom
            left:   root.left
            right:  root.right
            bottomMargin: units.gu(6)
        }
        currentIndex: -1
        model: entries.filteredEntryModel
        delegate: ListItemEntry{}
    }

    InputRow{
        id: inputRow
        width: parent.width
        anchors.top: sections.bottom
        placeholderText: i18n.tr("new entry ...")
        enabled: sections.selectedIndex>0
        onAccepted: {
            if (text.trim() !== ""){
                entries.insert(text.trim(),inputRow.quantity,inputRow.dimension)
                db_history.addKey(text.trim())
                reset()
            }
        }
    }

    ClearListButtons{
        id: clearList
        hasItems:        listView.model.count>0
        hasDeletedItems: entries.hasDeleted
        hasCheckedItems: entries.hasChecked
        onRemoveDeleted:  entries.removeDeleted()
        onRemoveAll:      entries.removeAll()
        onRemoveSelected: entries.removeSelected()
        onRestoreDeleted: entries.restoreDeleted()
    }

    // Navigation buttons to switch between categories
    Button{
        id: btPrevious
        anchors{
            bottom: parent.bottom
            left: parent.left
            margins: units.gu(2)
        }
        height: units.gu(4)
        width:  units.gu(6)
        visible: sections.selectedIndex>0
        onClicked: sections.selectedIndex -= 1
        Icon{
            anchors.centerIn: parent
            height: 0.7*parent.height
            name: "previous"
            color: theme.palette.normal.baseText
        }
    }
    Button{
        id: btNext
        anchors{
            bottom: parent.bottom
            right: parent.right
            margins: units.gu(2)
        }
        height: units.gu(4)
        width:  units.gu(6)
        visible: sections.selectedIndex <sections.model.length-1
        onClicked: sections.selectedIndex += 1
        Icon{
            anchors.centerIn: parent
            height: 0.7*parent.height
            name: "next"
            color: theme.palette.normal.baseText
        }
    }

}
