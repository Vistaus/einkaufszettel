/*
 * Copyright (C) 2020-2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * einkaufszettel is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import MDTK 1.0 as MDTK
import MDTK.Settings 1.0 as MDTKSettings

MDTK.ThemedFlickablePage {
    id: root
    title: i18n.tr("Shopping Lists") + " - " + i18n.tr("Settings")

    MDTKSettings.SettingsMenuItem{
        id: stManual
        text: i18n.tr("Show manual")
        iconName: "info"
        onClicked: manual.open()
    }

    MDTKSettings.SettingsMenuItem{
        id: stCategories
        text: i18n.tr("Edit categories")
        iconName: "edit"
        subpage: "file:qml/pages/SettingsCategoriesPage.qml"
    }
    MDTKSettings.SettingsMenuItem{
        id: stUnits
        text: i18n.tr("Edit units")
        iconName: "edit"
        subpage: "file:qml/pages/SettingsUnitsPage.qml"
    }

    MDTKSettings.SettingsCaption{title: i18n.tr("Suggestions")}
    MDTKSettings.SettingsMenuSwitch{
        id: stHistoryEnabled
        text: i18n.tr("Show suggestions")
        Component.onCompleted: checked = db_history.active
        onCheckedChanged: db_history.active = checked
    }
    MDTKSettings.SettingsMenuSwitch{
        id: stAcceptOnClicked
        text: i18n.tr("Insert suggestion on click")
        Component.onCompleted: checked = db_history.acceptOnClick
        onCheckedChanged: db_history.acceptOnClick = checked
    }
    MDTKSettings.SettingsMenuItem{
        id: stHistory
        text: i18n.tr("Edit history")
        iconName: "edit"
        subpage: "file:qml/pages/SettingsHistoryPage.qml"
    }

    MDTKSettings.SettingsCaption{title: i18n.tr("Appearance")}
    MDTKSettings.SettingsMenuSwitch{
        text: i18n.tr("Show check boxes to the right side")
        checked: settings.swapCheckBoxPosition
        onCheckedChanged: settings.swapCheckBoxPosition = checked
    }
    MDTKSettings.SettingsMenuSwitch{
        text: i18n.tr("Use default theme")
        checked: colors.useDefaultTheme
        onCheckedChanged: colors.useDefaultTheme = checked

    }
    MDTKSettings.SettingsMenuSwitch{
        enabled: !colors.useDefaultTheme
        text: i18n.tr("Dark Mode")
        onCheckedChanged: colors.darkMode = checked
        Component.onCompleted: checked = colors.darkMode
    }
    MDTKSettings.SettingsMenuDoubleColorSelect{
        enabled: !colors.useDefaultTheme
        text: i18n.tr("Color")
        model: colors.headerColors
        Component.onCompleted: currentSelectedColor =  colors.currentIndex
        onCurrentSelectedColorChanged: colors.currentIndex = currentSelectedColor
    }
}
