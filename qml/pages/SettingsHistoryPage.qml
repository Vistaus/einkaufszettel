/*
 * Copyright (C) 2020-2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * einkaufszettel is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Lomiri.Components 1.3
import MDTK 1.0 as MDTK

import "../components"

MDTK.ThemedPage {
    id: root
    title: i18n.tr("Shopping Lists") + " - " + i18n.tr("History")
    headerTrailingActionBar.actions: [
        Action{
            iconName: "select-none"
            visible: db_history.hasMarkedKeys
            onTriggered: db_history.deselectAll()
        }
    ]

    Label{
        anchors.centerIn: parent
        text: "("+i18n.tr("No entries") +")"
        visible: listView.model.count===0
    }

    LomiriListView{
        id: listView
        anchors{
            fill: parent
            topMargin: root.header.height
            bottomMargin: units.gu(4)
        }
        currentIndex: -1
        model: db_history.presortedKeyModel
        delegate: ListItemHistory{}
    }

    ClearListButtons{
        hasItems:        listView.model.count>0
        hasCheckedItems: db_history.hasMarkedKeys
        hasDeletedItems: db_history.hasDeletedKeys
        onRemoveAll:      db_history.markAllForDelete()
        onRemoveSelected: db_history.markSelectedForDelete()
        onRemoveDeleted:  db_history.removeDeleted()
        onRestoreDeleted: db_history.restore()
    }
}
