/*
 * Copyright (C) 2020-2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * einkaufszettel is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Lomiri.Components 1.3

ListItem{

    property bool isChecked: marked
    onIsCheckedChanged: checkBox.checked = marked

    leadingActions: ListItemActions{
        actions: [
            Action{
                iconName: "delete"
                onTriggered: db_categories.remove(index)
            }
        ]
    }

    Rectangle{
        anchors.fill: parent
        color: theme.palette.normal.positive
        opacity: 0.2
        visible: marked
    }

    ListItemLayout{
        id: layout
        CheckBox{
            id: checkBox
            SlotsLayout.position: settings.swapCheckBoxPosition ? SlotsLayout.Trailing : SlotsLayout.Leading
            checked: marked
            onTriggered: db_categories.toggleMarked(index)
        }

        title.text: name

        Icon{
            id: iconDragDrop
            SlotsLayout.position: settings.swapCheckBoxPosition ? SlotsLayout.Leading : SlotsLayout.Trailing
            height: units.gu(3)
            name: "sort-listitem"
            MouseArea{
                id: dragMouse
                anchors{
                    fill: parent
                    margins: units.gu(-1)
                }
                drag.target: layout
            }
        }

        property int dragItemIndex: index

        states: [
            State {
                when: layout.Drag.active
                ParentChange {
                    target: layout
                    parent: listView
                }
            },
            State {
                when: !layout.Drag.active
                AnchorChanges {
                    target: layout
                    anchors.horizontalCenter: layout.parent.horizontalCenter
                    anchors.verticalCenter: layout.parent.verticalCenter
                }
            }
        ]
        Drag.active: dragMouse.drag.active
        Drag.hotSpot.x: layout.width / 2
        Drag.hotSpot.y: layout.height / 2
    }

    DropArea{
        anchors.fill: parent
        onEntered: {
            if (drag.source.dragItemIndex > index){
                db_categories.swap(drag.source.dragItemIndex,index)
            } else if (drag.source.dragItemIndex < index){
                db_categories.swap(index,drag.source.dragItemIndex)
            }
        }
    }
}
