/*
 * Copyright (C) 2020-2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * einkaufszettel is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Lomiri.Components 1.3

ListItem{

    property bool isChecked: marked
    onIsCheckedChanged: checkBox.checked = marked

    leadingActions: ListItemActions{
        actions: [
            Action{
                iconName: "delete"
                onTriggered: db_history.deleteKey(key)
            }
        ]
    }

    Rectangle{
        anchors.fill: parent
        color: theme.palette.normal.positive
        opacity: 0.2
        visible: marked
    }

    ListItemLayout{
        title.text: key

        Label {
            id: lbCount
            SlotsLayout.position: SlotsLayout.Trailing
            horizontalAlignment: Label.AlignHCenter
            text: "("+count+")"
        }

        CheckBox{
            id: checkBox
            SlotsLayout.position: settings.swapCheckBoxPosition ? SlotsLayout.Trailing : SlotsLayout.Leading
            checked: marked
            onTriggered: db_history.toggleMarked(key)
        }
    }
}
