/*
 * Copyright (C) 2020-2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * einkaufszettel is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Lomiri.Components 1.3
import Lomiri.Components.Pickers 1.3
import Lomiri.Components.Popups 1.3

Label {
    id: root

    property int quantity: 1
    property int dimensionIndex: 0
    property int quantityDigits: 4

    function reset(){
        quantity = 1
        dimensionIndex = 0
        dimensions.init()
    }

    function open(){
        if (!opened)
            PopupUtils.open(popoverComponent,root)
    }

    text: quantity + " " + dimensions.unitsModel.get(root.dimensionIndex).symbol

    readonly property MouseArea mouse: MouseArea {
        anchors.fill: parent
    }

    property bool opened: false

    Component{
        id: popoverComponent
        Popover{
            id: popover
            width: row.width
            Component.onDestruction: root.opened = false

            property bool initialised: false

            Component.onCompleted: {
                root.opened = true
                var q = root.quantity
                for (var i=0; i<root.quantityDigits; i++)
                    quantityPickers.itemAt(i).selectedIndex = (q / Math.pow(10,root.quantityDigits-i-1)) % 10
                dimensionPicker.selectedIndex = root.dimensionIndex
                initialised = true
            }

            function updateQuantity(){
                if (initialised){
                    var q = 0
                    for (var i=0; i<root.quantityDigits; i++)
                        q += quantityPickers.itemAt(i).selectedIndex * Math.pow(10,root.quantityDigits-i-1)
                    root.quantity = q
                }
            }

            function updateDimension(){
                if (initialised)
                    root.dimensionIndex = dimensionPicker.selectedIndex
            }

            Row{
                id: row
                padding: units.gu(2)
                spacing: units.gu(1)

                Row{
                    Repeater{
                        id: quantityPickers
                        model: root.quantityDigits
                        delegate: Picker{
                            model: 10
                            width: units.gu(4)
                            onSelectedIndexChanged: popover.updateQuantity()
                            delegate: PickerDelegate{
                                Label{
                                    anchors.centerIn: parent
                                    text: modelData
                                }
                            }
                        }
                    }
                }

                Rectangle{
                    anchors.verticalCenter: dimensionPicker.verticalCenter
                    height: dimensionPicker.height - units.gu(4)
                    width: units.gu(0.25)
                    color: theme.palette.normal.base
                }

                Picker{
                    id: dimensionPicker
                    model: dimensions.unitsModel
                    width: units.gu(6)
                    circular: false
                    onSelectedIndexChanged: popover.updateDimension()
                    delegate: PickerDelegate{
                        Label{
                            anchors.centerIn: parent
                            text: symbol
                        }
                    }
                }

                Button{
                    height: dimensionPicker.height
                    width: units.gu(7)
                    text: i18n.tr("OK")
                    color: LomiriColors.orange
                    onClicked: PopupUtils.close(popover)
                }
            }
        }
    }
}
