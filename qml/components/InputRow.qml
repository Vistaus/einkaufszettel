/*
 * Copyright (C) 2020-2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * einkaufszettel is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Lomiri.Components 1.3
import Lomiri.Components.ListItems 1.3
import Lomiri.Components.Popups 1.3

Row{
    id: root

    property string placeholderText: ""
    property string text: input.displayText
    property bool enabled: true

    property alias  quantity: quantitySelect.quantity
    property string dimension: dimensions.unitsModel.get(quantitySelect.dimensionIndex).symbol


    SortFilterModel{
        id: filterModel
        model: db_history.sortedKeyModel
        filter.property: "key"
        filter.pattern: new RegExp(input.displayText)
        filterCaseSensitivity: Qt.CaseInsensitive
    }

    signal accepted()
    function reset(){
        input.text = ""
        quantitySelect.reset()
    }

    property int dropDownRowHeight: units.gu(5)
    property int dropDownMaxRows: 5

    padding: units.gu(2)
    spacing: units.gu(1)

    Button{
        id: quantitySelectButton
        width: Math.max(quantitySelect.width + units.gu(2),units.gu(5))
        enabled: root.enabled

        QuantitySelect{
            id: quantitySelect
            anchors.centerIn: parent
            horizontalAlignment: Label.AlignHCenter
            mouse.onClicked: open()
        }

        onClicked: quantitySelect.open()
    }

    TextField{
        id: input
        width: root.width - button.width - 2*inputRow.padding - units.gu(2) - quantitySelectButton.width
        placeholderText: root.placeholderText
        enabled: root.enabled
        onAccepted: root.accepted()

        onFocusChanged: {
            if (focus)
                if (text.length>0) dropDown.visible = true
            else
                dropDown.visible = false
        }
        onDisplayTextChanged:  dropDown.visible = (db_history.active && displayText.length>0)

        Rectangle{
            id: dropDown
            anchors{
                left:  input.left
                right: input.right
                top:   input.bottom
            }
            height: (dropDownList.model.count<dropDownMaxRows ? dropDownList.model.count : dropDownMaxRows)*dropDownRowHeight
            visible: false

            color: theme.palette.normal.field
            border.width: 1
            border.color: theme.palette.normal.base
            radius: units.gu(1)

            LomiriListView{
                id: dropDownList
                anchors.fill: parent
                model: filterModel
                clip: true
                currentIndex: -1
                delegate: ListItem{
                    height: root.dropDownRowHeight
                    Label{
                        anchors.centerIn: parent
                        text: key
                    }
                    onClicked: {
                        input.text = key
                        dropDown.visible = false
                        if (db_history.acceptOnClick) root.accepted()
                    }
                }
            }
        }
    }

    Button{
        id: button
        color: theme.palette.normal.positive
        width: 1.6*height
        Icon{
            anchors.centerIn: parent
            height: 0.7*button.height
            name: "add"
            color: theme.palette.normal.positiveText
        }

        enabled: root.enabled
        onClicked: root.accepted()
    }
}
