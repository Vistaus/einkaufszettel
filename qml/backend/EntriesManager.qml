/*
 * Copyright (C) 2020-2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * einkaufszettel is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import "db"

QtObject{
    id: root

    property var db: EntriesDatabase{}

    property var filteredEntryModel: ListModel{}
    property var fullEntryModel:     ListModel{}

    property bool hasChecked: false
    property bool hasDeleted: false

    signal itemAdded(var item)
    signal itemRemoved(var item)

    property string selectedCategory: ""
    property bool showCategoryOther: false

    Component.onCompleted: {
        const rows = db.readEntries()
        for (var i=0; i<rows.length; i++){
            fullEntryModel.append(rows[i])
            itemAdded(rows[i])
        }
        internal.refreshFilteredEntryModel()
    }

    function filteredIndexByUid(uid){
        for (var i=0; i<filteredEntryModel.count; i++)
            if (filteredEntryModel.get(i).uid === uid)
                return i
        return -1
    }
    function fullIndexByUid(uid){
        for (var i=0; i<fullEntryModel.count; i++)
            if (fullEntryModel.get(i).uid === uid)
                return i
        return -1
    }

    function updateSelectedCategory(catName,isOther){
        if (selectedCategory === catName && showCategoryOther === isOther)
            return

        selectedCategory  = catName
        showCategoryOther = isOther
        internal.refreshFilteredEntryModel()
    }

    property var internal: QtObject{

        function checkForMarkedEntries(){
            for (var i=0; i<root.filteredEntryModel.count; i++)
                if (root.filteredEntryModel.get(i).marked === 1){
                    root.hasChecked = true
                    return
                }
            root.hasChecked = false
        }

        function refreshFilteredEntryModel(){
            var i
            root.filteredEntryModel.clear()
            if (root.showCategoryOther){
                for (i=0; i<root.fullEntryModel.count; i++)
                    if (!db_categories.exists(root.fullEntryModel.get(i).category))
                        insertToFiltered(root.fullEntryModel.get(i))
            } else {
                if (root.selectedCategory === "")
                    for (i=0; i<root.fullEntryModel.count; i++)
                            insertToFiltered(root.fullEntryModel.get(i))
                else
                    for (i=0; i<root.fullEntryModel.count; i++)
                        if (root.fullEntryModel.get(i).category === root.selectedCategory)
                            insertToFiltered(root.fullEntryModel.get(i))
            }
            checkForMarkedEntries()
        }

        function insertToFiltered(item){
            var j=0
            while (j < root.filteredEntryModel.count &&
                   root.filteredEntryModel.get(j).rank < item.rank &&
                   root.filteredEntryModel.get(j).rank > -1)
                j++
            root.filteredEntryModel.insert(j,item)
        }

        function removeFromModels(uid){
            var fullIndex = root.fullIndexByUid(uid)
            root.itemRemoved(root.fullEntryModel.get(fullIndex))
            root.fullEntryModel.remove(fullIndex)

            var filteredIndex = root.filteredIndexByUid(uid)
            if (filteredIndex > -1){
                root.filteredEntryModel.remove(filteredIndex)
                checkForMarkedEntries()
            }
        }
    }

    function insert(name,quantity,dimension){
        // check if an item with the same name,category and dimension exists already
        var index = -1
        for (var i=0; i<filteredEntryModel.count; i++){
            var item = filteredEntryModel.get(i)
            if (item.name === name && item.dimension === dimension){
                index = i
                break
            }
        }

        if (index > -1){
            // if there is a match, add quantity to this entry
            var match = filteredEntryModel.get(index)
            var newQuantity = match.quantity+quantity
            db.updateQuantity(match.uid,newQuantity)
            filteredEntryModel.setProperty(index,"quantity",newQuantity)
            for (var j=0; j<fullEntryModel.count; j++){
                if (fullEntryModel.get(j).uid === match.uid){
                    fullEntryModel.setProperty(j,"quantity",newQuantity)
                    break
                }
            }
        } else {
            // create a new entry otherwise
            var newItem = {
                uid: 0, // to be set by database
                name: name,
                category: showCategoryOther ? i18n.tr("other") : selectedCategory,
                deleteFlag: 0,
                dimension: dimension,
                quantity: quantity,
                marked: 0,
                rank: 0
            }

            if (db.insert(newItem)){
                // increment ranks of all entries
                for (var k=0; k< filteredEntryModel.count; k++)
                    filteredEntryModel.get(k).rank += 1
                for (var l=0; l< fullEntryModel.count; l++)
                    fullEntryModel.get(l).rank += 1

                // insert new item
                fullEntryModel.insert(0,newItem)
                if (selectedCategory === "" || selectedCategory === newItem.category)
                    filteredEntryModel.insert(0,newItem)
                itemAdded(newItem)
            }
        }
    }

    function remove(uid){
        if (db.remove(uid))
            internal.removeFromModels(uid)
    }

    function removeAll(){
        for (var i=filteredEntryModel.count-1; i>-1; i--){
            var item = filteredEntryModel.get(i)
            if (db.markAsDeleted(item.uid)){
                internal.removeFromModels(item.uid)
                hasDeleted = true
            }
        }
        hasChecked = false
    }

    function removeSelected(){
        for (var i=filteredEntryModel.count-1; i>-1; i--){
            var item = filteredEntryModel.get(i)
            if (item.marked === 0)
                continue

            if (db.markAsDeleted(item.uid)){
                internal.removeFromModels(item.uid)
                hasDeleted = true
            }
        }
        hasChecked = false
    }
    function removeDeleted(){
        if (db.removeDeleted())
            hasDeleted = false
    }
    function restoreDeleted(){
        const  restored = db.restoreDeleted()

        for (var i=0; i<restored.length; i++){
            var item = restored[i]
            item.deleteFlag = 0
            fullEntryModel.insert(0,item)
            var catExist = db_categories.exists(item.category)
            if ((selectedCategory === "" && !showCategoryOther)
                    || (!catExist && showCategoryOther)
                    || (item.category === selectedCategory))
                internal.insertToFiltered(item)
            itemAdded(item)
        }

        if (restored.length>0)
            internal.checkForMarkedEntries()
        hasDeleted = false
    }

    function updateQuantity(uid,quantity){
        const idx = fullIndexByUid(uid)
        if (idx < 0)
            return

        var item = fullEntryModel.get(idx)
        if (item.quantity !== quantity){
            if (db.updateQuantity(uid,quantity)){
                item.quantity = quantity
                const filteredIdx = filteredIndexByUid(uid)
                if (filteredIdx > -1)
                    filteredEntryModel.get(filteredIdx).quantity = quantity
            }
        }
    }
    function updateDimension(uid,dimension){
        const idx = fullIndexByUid(uid)
        if (idx < 0)
            return

        var item = fullEntryModel.get(idx)
        if (item.dimension !== dimension){
            if (db.updateDimension(uid,dimension)){
                item.dimension = dimension
                const filteredIdx = filteredIndexByUid(uid)
                if (filteredIdx > -1)
                    filteredEntryModel.get(filteredIdx).dimension = dimension
            }
        }
    }

    function toggleMarked(uid){
        if (db.toggleMarked(uid)){
            var fullIndex = fullIndexByUid(uid)
            var marked = 1-fullEntryModel.get(fullIndex).marked
            fullEntryModel.setProperty(fullIndex,"marked",marked)

            var filteredIndex = filteredIndexByUid(uid)
            if (filteredIndex > -1)
                filteredEntryModel.setProperty(filteredIndex,"marked",marked)
            internal.checkForMarkedEntries()
        }
    }
    function deselectAll(){
        for (var i=0;i<filteredEntryModel.count;i++){
            var item = filteredEntryModel.get(i)
            if (item.marked===1)
                toggleMarked(item.uid)
        }
    }

    function swap(uid1,uid2){
        // check if both items exists
        const fullIndex1 = fullIndexByUid(uid1)
        const fullIndex2 = fullIndexByUid(uid2)
        if (fullIndex1 === -1 || fullIndex2 === -1)
            return

        var item1 = fullEntryModel.get(fullIndex1)
        var item2 = fullEntryModel.get(fullIndex2)

        if (db.swap(item1,item2)){
            // swap in full model
            const rank1 = item1.rank
            const rank2 = item2.rank
            item1.rank = rank2
            item2.rank = rank1

            fullEntryModel.move(fullIndex1,fullIndex2,1)

            // swap in filtered model
            const filteredIndex1 = filteredIndexByUid(uid1)
            const filteredIndex2 = filteredIndexByUid(uid2)
            if (filteredIndex1 > -1)
                filteredEntryModel.get(filteredIndex1).rank = rank2
            if (filteredIndex2 > -1)
                filteredEntryModel.get(filteredIndex2).rank = rank1
            if (filteredIndex1 > -1 && filteredIndex2 > -1)
                filteredEntryModel.move(filteredIndex1,filteredIndex2,1)
        }
    }
}
