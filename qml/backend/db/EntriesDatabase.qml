/*
 * Copyright (C) 2020-2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * einkaufszettel is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import QtQuick.LocalStorage 2.12 as Sql

QtObject {
    id: root

    // connection details
    property var db
    property string db_name:        "einkauf"
    property string db_version:     "1.0"
    property string db_description: "DB of Einkaufszettel app"
    property int    db_size:        1024
    property string db_table_name:  "items"

    function db_test_callback(db){/* do nothing */}
    function init(){
        // open database
        db = Sql.LocalStorage.openDatabaseSync(db_name,db_version,db_description,db_size,db_test_callback(db))

        // Create items table if needed
        try{
            db.transaction(function(tx){
                tx.executeSql("CREATE TABLE IF NOT EXISTS "+db_table_name+" "
                              +"(uid INTEGER PRIMARY KEY AUTOINCREMENT"
                              +",name TEXT"
                              +",category TEXT"
                              +",deleteFlag INTEGER DEFAULT 0"
                              +",rank INTEGER DEFAULT -1"
                              +")"
                             )
            })
        } catch (e1){
            console.error("Error when creating table '"+db_table_name+"': " + e1)
        }

        // check if all required colunms are in table and create missing ones
        try{
            var colnames = []
            db.transaction(function(tx){
                var rt = tx.executeSql("PRAGMA table_info("+db_table_name+")")
                for(var i=0;i<rt.rows.length;i++){
                    colnames.push(rt.rows[i].name)
                }
            })
            // since v1.0.2:
            if (colnames.indexOf("deleteFlag")<0){
                db.transaction(function(tx){
                    tx.executeSql("ALTER TABLE "+db_table_name+" ADD deleteFlag INTEGER DEFAULT 0")
                })
            }
            // since v1.3.0:
            if (colnames.indexOf("dimension")<0){
                db.transaction(function(tx){
                    tx.executeSql("ALTER TABLE "+db_table_name+" ADD dimension TEXT DEFAULT 'x'")
                })
            }
            if (colnames.indexOf("quantity")<0){
                db.transaction(function(tx){
                    tx.executeSql("ALTER TABLE "+db_table_name+" ADD quantity INT DEFAULT 1")
                })
            }
            // since v1.3.1:
            if (colnames.indexOf("marked")<0){
                db.transaction(function(tx){
                    tx.executeSql("ALTER TABLE "+db_table_name+" ADD marked INT DEFAULT 0")
                })
            }
            // since v1.4.0:
            if (colnames.indexOf("rank")<0){
                db.transaction(function(tx){
                    tx.executeSql("ALTER TABLE "+db_table_name+" ADD rank INT DEFAULT -1")
                })
            }
        } catch (e2){
            console.error("Error when checking columns of table '"+db_table_name+"': " + e2)
        }

        // if there are still deleted items left, remove them from DB for a clean start
        removeDeleted()
    }

    function readEntries() {
        if (!db) init()

        var rows
        try{
            db.transaction(function(tx){
                rows = tx.executeSql("SELECT * FROM "+db_table_name+" WHERE deleteFlag=0").rows
            })
        } catch (e3){
            console.error("Error when selecting all entries: " + e3)
            return []
        }
        return rows
    }

    function insert(item){
        if (!db) init()

        try{
            db.transaction(function(tx){
                // increment ranks of all entries
                tx.executeSql("UPDATE "+db_table_name+" SET rank=rank+1 ")

                // insert new entry on top of list (rank=0)
                item.uid = parseInt(tx.executeSql("INSERT INTO "+db_table_name+" (name,category,quantity,dimension,rank) VALUES (?,?,?,?,0)"
                                                 ,[item.name,item.category,item.quantity,item.dimension]).insertId)
            })
        } catch (err){
            console.error("Error when insert item: " + err)
            return false
        }
        return true
    }

    function updateQuantity(uid,quantity){
        if (!db) init()

        db.transaction(function(tx){
            tx.executeSql("UPDATE "+db_table_name+" SET quantity = ? WHERE uid=?",[quantity,uid])
            return false
        })
        return true
    }
    function updateDimension(uid,dimension){
        if (!db) init()

        db.transaction(function(tx){
            tx.executeSql("UPDATE "+db_table_name+" SET dimension = ? WHERE uid=?",[dimension,uid])
            return false
        })
        return true
    }

    function remove(uid){
        if (!db) init()

        try{
            db.transaction(function(tx){
                tx.executeSql("DELETE FROM "+db_table_name+" WHERE uid='"+uid+"'")
            })
        } catch (err){
            console.error("Error when delete entry: " + err)
            return false
        }
        return true
    }

    function markAsDeleted(uid){
        if (!db) init()

        try{
            db.transaction(function(tx){
                tx.executeSql("UPDATE "+db_table_name+" SET deleteFlag=deleteFlag+1 WHERE uid="+uid)
            })
        } catch (err){
            console.error("Error when mark entry as deleted: " + err)
            return false
        }
        return true
    }
    function removeDeleted(){
        if (!db) init()

        try{
            db.transaction(function(tx){
                tx.executeSql("DELETE FROM "+db_table_name+" WHERE deleteFlag>0")
            })
        } catch (err){
            console.error("Error when remove deleted from table '"+db_table_name+"': " + err)
            return false
        }
        return true
    }
    function restoreDeleted(){
        if (!db) init()

        var restored
        try{
            db.transaction(function(tx){
                restored = tx.executeSql("SELECT * FROM "+db_table_name+" WHERE deleteFlag>0").rows
            })
            // for some reason, when restored is not called in this try-block, only one entry would be returned.
            // But having this log entry makes the function to return the full restored list.
            console.info("EntriesDatabase: restored",restored.length,"entries in DB.")
            db.transaction(function(tx){
                tx.executeSql("UPDATE "+db_table_name+" SET deleteFlag=0")
            })
        } catch (err){
            console.error("Error when restoring deleted entries in table '"+db_table_name+"': " + err)
            return []
        }
        return restored
    }

    function toggleMarked(uid){
        if (!db) init()

        try{
            db.transaction(function(tx){
                tx.executeSql("UPDATE "+db_table_name+" SET marked=1-marked WHERE uid=?",
                              [uid])
            })
        } catch (err){
            console.error("Error when toggle marked property of uid="+uid+": " + err)
            return false
        }
        return true
    }

    function swap(item1,item2){
        if (!db) init()

        try{
            db.transaction(function(tx){
                tx.executeSql("UPDATE "+db_table_name+" SET rank=? WHERE uid=?",
                              [item2.rank,item1.uid])
                tx.executeSql("UPDATE "+db_table_name+" SET rank=? WHERE uid=?",
                              [item1.rank,item2.uid])
            })
        } catch (err){
            console.error("Error when swaping items in table '"+db_table_name+"': " + err)
            return false
        }
        return true
    }
}
